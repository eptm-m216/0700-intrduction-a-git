# 0700 Intrduction à git

## Objectifs

* Créer un dépôt Git
* Effectuer un commit et un push
* Créer une branche
* Fusionner des branches

## Ressources 

* [Documentation Git](https://git-scm.com/book/en/v2)

## Consignes

* Votre `<visa>` est toujours composé des 4 premières lettres de votre prénom et des 4 premières lettres de votre nom

## Installation

```bash
git config --global user.name "<Prénom> <Nom>"
git config --global user.email "<prenom>.<nom>@edu.vs.ch"
```

## Instructions 

### 1. Initialiser un dépôt Git

```
git init -b main
```

### 2. Ajouter un fichier Python

- Créez un fichier `hello_world.py` avec le code suivant :

```python
print("Hello World!")
```

### 3. Ajouter et commiter le fichier
- Pour chaque étape, soyez attentif au résultat de la commande `git status`
```bash
git status
git add hello_world.py
git status
git commit -m "Ajout de hello_world.py"
git status
```

### 4. Créer une branche

```bash
git checkout -b feature
```

### 5. Modifier le script Python

- Modifiez le message `"Hello World!"` en  `"Hello <visa>!"` dans le fichier `hello_world.py`.

### 6. Ajouter et commiter les modifications

```bash
git add hello_world.py
git commit -m "Modification du message Hello World"
```

### 7. Consulter  l'historique

```bash
git log
```

### 8. Revenir sur la branche principale

```
git checkout main
git log
```

### 9. Fusionner les deux branche

```bash
git merge feature
git log
```

### 10.  Créer un compte GitLab

- [Créez un compte gitlab](https://gitlab.com/users/sign_up)
- Le nom du **groupe** est `<yyyy>_M216_<visa>` yyyy = Année scolaire en cours, exemple 2425
- **Décochez la case `Initialize repository with a README` dans `Project Configuration`.**
![Readme](img/readme.png)
- Le nom du projet est `0700 Introduction`

### 11. Ajouter le dépôt distant au dépôt local
- Copiez le lien du dépôt grâce au bouton `Code\Clone with HTTPS` ![Clone with HTTPS](img/url.png)
```bash
git remote add origin <lien du dépôt>
git branch --set-upstream-to=origin/main
```


### 12. Pousser les modifications vers le dépôt distant
```bash
git push --set-upstream origin main
```
- Rafraîchissez votre navigateur et vous verrez désormais votre fichier `hello_world.py` sur votre dépôt distant.

### 11.  Ajouter un utilisateur
- Dans les options du **groupe**, ajoutez `@jeremy.michaud.eptm` comme `Reporter`
- Vérifier avec l'enseignant que l'ajout a fonctionné

## Vérification :

- Assurez-vous que le fichier `hello_world.py` contient le message modifié.
- Vérifiez l'historique des commits pour voir les différentes modifications.

## Bonus :

- Essayez de créer un conflit de fusion en modifiant le même fichier dans les deux branches.
- Apprenez à résoudre les conflits de fusion.